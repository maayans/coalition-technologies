<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'ProductsController@index']);

Route::post('products/', ['as' => 'products.store', 'uses' => 'ProductsController@store']);

Route::post('products/{id}/', ['as' => 'products.edit', 'uses' => 'ProductsController@update']);
