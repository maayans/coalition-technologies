/*
* Form for create a new product
*/

$('#add_product_form').submit(function(e){
  e.preventDefault(e);
  $.ajaxSetup({
    header:$('meta[name="csrf-token"]').attr('content')
  });
  $.ajax({
    url: '/products',
    data:$(this).serialize(),
    type: "POST",
    dataType: 'json',
    success: function(result){

      var products = $('#productsTable');
      var product_total_value = result.quantity_in_stock * result.price_per_item;
      var products_total_value = $("#totalValue");

      var product = '<tr>';
      product += '<td>'+result.id+'</td>';
      product += '<td>'+result.name+'</td>';
      product += '<td>'+result.quantity_in_stock+'</td>';
      product += '<td>'+result.price_per_item+'</td>';
      product += '<td>'+result.created_at+'</td>';
      product += '<td>'+product_total_value+'</td>';
      product += '<td><button type="button" class="btn btn-warning" data-toggle="modal" onclick="openEditModal($(this))">Edit</button></td>';
      product += '</tr>';

      products.append(product);

      var total_value = parseInt(products_total_value.text());
      total_value += product_total_value;
      products_total_value.html(total_value);
    }
  })
});

/*
* Open the edit product modal
*/
function openEditModal(ele){
  var currentRow=ele.closest("tr");

  var id = currentRow.find("td:eq(0)").text(); // get current row 1st TD value
  var name=currentRow.find("td:eq(1)").text(); // get current row 2nd TD
  var quantity_in_stock=currentRow.find("td:eq(2)").text(); // get current row 3rd TD
  var price_per_item=currentRow.find("td:eq(3)").text(); // get current row 4rd TD

  var data = '<div class="form-group">';

  data +='<form id="editProductForm">';

  data += '<div class="form-group">';
  data += '<label>Name</label>';
  data += '<input class="form-control" name="name" type="text" id="editName" value="'+name+'">';
  data += '</div>';

  data += '<div class="form-group">';
  data += '<label>Quantity in stock</label>';
  data += '<input class="form-control" name="quantity_in_stock" id="editQuantity" type="number" value="'+quantity_in_stock+'">';
  data += '</div>';

  data += '<div class="form-group">';
  data += '<label>Price per item</label>';
  data += '<input class="form-control" name="price_per_item" type="number" id="editPrice" value="'+price_per_item+'">';
  data += '</div>';

  data += '<input id="editProductId" class="form-control" type="hidden" value="'+id+'">';

  data += '</form>';

  data += '</div>';

  $('#modalBody').html(data);

  $('#myModal').modal('toggle');
}

/*
* Update product
*/
$('button#productEditSubmit').click( function(e) {
  e.preventDefault(e);
  var id = $("#editProductId").val();
  var token = $('meta[name="csrf-token"]').attr('content');

  $.ajax({
    url: '/products/'+id,
    type: 'POST',
    dataType: 'json',
    data: {name:$('#editName').val(),
            quantity_in_stock:$('#editQuantity').val(),
            price_per_item:$('#editPrice').val(),
            '_token':token
          },
    success: function() {
      location.reload();
    }
  });

});