<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ContactRequest
 * @package App\Http\Requests\Admin
 */
class ProductRequest extends FormRequest {
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => 'required|max:255',
      'quantity_in_stock' => 'required|numeric',
      'price_per_item' => 'required|numeric',
    ];
  }

  public function authorize()
  {
    return true;
  }
}