<?php

namespace App\Http\Controllers;


use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller {

  /**
   * Display a listing of the resource.
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $products = Product::orderBy('created_at', 'asc')->get();

    $total_value = Product::all()->sum(function($product){
      return $product->quantity_in_stock * $product->price_per_item;
    });

    return view('welcome',compact('products','total_value'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param ProductRequest|Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(ProductRequest $request)
  {

    $data = [
      'name'              => $request->name,
      'quantity_in_stock' => $request->quantity_in_stock,
      'price_per_item'    => $request->price_per_item,
    ];

    $product = Product::create($data);

    $product->update($data);

    return response()->json($product);

  }

  /**
   * Update the specified resource in storage.
   *
   * @param ProductRequest|Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(ProductRequest $request, $id)
  {

    $data = [
      'name'              => $request->name,
      'quantity_in_stock' => $request->quantity_in_stock,
      'price_per_item'    => $request->price_per_item,
    ];

    $product = Product::findOrFail($id);
    $product->update($data);

    return response()->json($product);
  }

}
